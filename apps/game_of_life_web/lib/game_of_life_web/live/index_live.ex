defmodule GameOfLifeWeb.IndexLive do
  use GameOfLifeWeb, :live_view

  @impl true
  def render(assigns) do
    ~H"""
    <section class="phx-hero">
      <h1><%= gettext "Game Of Life!" %></h1>
    </section>
    <section class="row">
      <article class="column">
        <.live_component module={GameOfLifeWeb.GameComponent} id="game" speed="1" />
      </article>
    </section>
    """
  end

  @impl true
  def mount(_params, _assigns, socket) do
    {:ok, socket}
  end
end
