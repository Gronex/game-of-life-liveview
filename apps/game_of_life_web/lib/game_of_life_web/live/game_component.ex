defmodule GameOfLifeWeb.GameComponent do
  use GameOfLifeWeb, :live_component

  require Logger

  @impl true
  def mount(socket) do
    state =
      socket
      |> assign(:cells, MapSet.new())
      |> assign(:generation, 0)
      |> assign(:running, false)

    {:ok, state}
  end

  @impl true
  def update(%{id: id, next_generation: true}, %{assigns: %{running: true, speed: speed}} = socket) do
    send_update_after(self(), GameOfLifeWeb.GameComponent, [id: id, next_generation: true], 1000 * get_number(speed))

    {:ok, next_generation(socket)}
  end

  @impl true
  def update(assigns, socket) do
    socket =
      socket
      |> assign(assigns)

    {:ok, socket}
  end

  @impl true
  def render(assigns) do
    ~H"""
    <div id="board">
      <%= settings(assigns) %>
      <div>
        <span><%= gettext "Generation: %{generation} ", generation: @generation %></span>
      </div>
      <div>
        <button phx-click="next_generation" phx-target={@myself}><%= gettext "Next" %></button>
        <button phx-click="reset" phx-target={@myself}><%= gettext "Reset" %></button>
        <%= if @running do %>
          <button phx-click="start_stop" phx-target={@myself}><%= gettext "Stop" %></button>
        <% else %>
          <button phx-click="start_stop" phx-target={@myself}><%= gettext "Start" %></button>
        <% end %>
      </div>
      <%= board(assigns) %>
    </div>
    """
  end

  @impl true
  def handle_event("change", %{"speed" => speed} = params, socket) do
    Logger.info("got change #{inspect(params)}")
    {speed, _} = Integer.parse(speed)
    {:noreply, assign(socket, :speed, speed)}
  end


  @impl true
  def handle_event("point_selected", %{"x" => x, "y" => y}, %{assigns: %{cells: cells}} = socket) do
    Logger.info("got point #{x}, #{y}")

    cells = if(MapSet.member?(cells, {x, y})) do
      MapSet.delete(cells, {x, y})
    else
      MapSet.put(cells, {x, y})
    end

    {:noreply, assign(socket, :cells, cells)}
  end

  @impl true
  def handle_event("start_stop", _, %{assigns: %{id: id, running: running}} = socket) do
    Logger.info("Setting running: #{!running}")

    send_update(self(), GameOfLifeWeb.GameComponent, [id: id, next_generation: true])

    {:noreply, assign(socket, :running, !running)}
  end

  @impl true
  def handle_event("next_generation", _, socket) do
    socket = socket
    |> next_generation()
    {:noreply, socket}
  end

  @impl true
  def handle_event("reset", _, socket) do
    generation = 0
    Logger.info("Generation: #{generation}")

    cells = MapSet.new()

    socket = socket
    |> assign(:cells, cells)
    |> assign(:generation, generation)

    {:noreply, socket}
  end

  def board(assigns) do
    ~H"""
    <gol-board id="board" rows="25" columns="50" phx-hook="GameBoard" phx-target={@myself}>
      <%= for {x, y} <- @cells do %>
        <gol-point x={x} y={y} />
      <% end %>
    </gol-board>
    """
  end

  def settings(assigns) do
    ~H"""
    <form class="hero" phx-change="change" phx-target={@myself}>
      <label for="speed"><%= gettext "Speed" %></label>
      <input name="speed" id="speed" type="number" min="0" value={@speed} />
    </form>
    """
  end

  defp next_generation(%{assigns: %{cells: cells, generation: generation}} = socket) do
    generation = generation + 1
    Logger.info("Generation: #{generation}")

    cells = cells
    |> GameOfLife.Game.next()

    socket
    |> assign(:cells, cells)
    |> assign(:generation, generation)
  end

  defp get_number(num) when is_integer(num), do: num
  defp get_number(num) when is_binary(num) do
    Integer.parse(num)
    |> then(fn {x, _} -> x end)
  end
end
