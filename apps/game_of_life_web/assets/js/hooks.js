export default {
    GameBoard: {
        mounted() {
            this.el.addEventListener('point_selected', event => {
                this.pushEventTo(this.el.attributes['phx-target'].value, 'point_selected', event.detail);
            })
        }
    }
}