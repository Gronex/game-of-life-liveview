import Point from "./point";

customElements.define('gol-point', Point);
export default class Board extends HTMLElement {

    // Specify observed attributes so that
    // attributeChangedCallback will work
    static get observedAttributes() {
        return ['columns', 'rows'];
    }

    #canvasContext;
    #width;
    #height;

    constructor() {
        // Always call super first in constructor
        super();

        const shadow = this.attachShadow({mode: 'open'});

        const canvas = document.createElement('canvas');
        this.#canvasContext = canvas.getContext("2d");
        const scale = window.devicePixelRatio;

        canvas.innerText = "No canvas :(";
        canvas.style.width = "100%";
        shadow.appendChild(canvas);
        
        this.#width = canvas.width = this.#getActualWidth(canvas) * scale;
        this.#height = canvas.height = this.#width / this.columns * this.rows;
        console.log("Scale:", scale);
        this.#canvasContext.scale(scale, scale);


        canvas.addEventListener('click', (event) => {
            const coords = getCursorPosition(this.#canvasContext.canvas, event);
            this.dispatchEvent(new CustomEvent('point_selected', {
                bubbles: true,
                detail: {
                    x: Math.floor(coords.x / this.squareSize),
                    y: Math.floor(coords.y / this.squareSize),
                }
            }));
        });

        requestAnimationFrame(this.renderFrame.bind(this));
    }

    attributeChangedCallback() {
        console.debug("AttributeChanged");
    }

    connectedCallback() {
        if(!this.isConnected){
            return;
        }
        console.debug("Connected", this);
        //this.#renderGrid();
    }

    renderFrame(timestamp) {
        this.#canvasContext.clearRect(0, 0, this.#canvasContext.canvas.width, this.#canvasContext.canvas.height)
        this.renderGrid();
        this.renderPoints();
        requestAnimationFrame(this.renderFrame.bind(this));
    }

    renderGrid() {
        for (let x = 0; x <= this.columns; x++) {
            const xPos = x * this.squareSize;
            this.#drawLine(xPos, 0, xPos, this.#height);
        }
        for (let x = 0; x <= this.rows; x++) {
            const yPos = x * this.squareSize;
            this.#drawLine(0, yPos, this.#width, yPos);
        }

        this.#canvasContext.strokeRect(0, 0, this.#width, this.#height);
    }

    get columns() {
        const num = Number.parseInt(this.getAttribute("columns") || 10);
        return num > 0 ? num : 10;
    }

    get rows() {
        const num = Number.parseInt(this.getAttribute("rows") || 10);
        return num > 0 ? num : 10;
    }

    get squareSize() {
        return Math.max(this.#width, this.#height) / Math.max(this.rows, this.columns);
    }

    #drawLine(fromX, fromY, toX, toY, width) {
        // offset by 0.5 to account for blurryness

        const previousWidth = this.#canvasContext.lineWidth;
        const previousStroke = this.#canvasContext.strokeStyle;

        this.#canvasContext.lineWidth = width ?? .1;
        this.#canvasContext.strokeStyle = 'black';

        this.#canvasContext.beginPath();
        this.#canvasContext.moveTo(fromX, fromY);
        this.#canvasContext.lineTo(toX, toY);
        this.#canvasContext.stroke();
        this.#canvasContext.closePath();
        
        this.#canvasContext.lineWidth = previousWidth;
        this.#canvasContext.strokeStyle = previousStroke;
    }

    renderPoints() {
        const points = this.getElementsByTagName('gol-point');
        for(let i = 0; i < points.length; i++) {
            const point = points.item(i);
            this.#canvasContext.fillRect(point.x * this.squareSize, point.y * this.squareSize, this.squareSize, this.squareSize);
        }
    }

    #getActualWidth(canvas) {
        return +getComputedStyle(canvas).getPropertyValue("width").slice(0, -2)
    }
}

function getCursorPosition(canvas, event) {
    const rect = canvas.getBoundingClientRect()
    const x = event.clientX - rect.left
    const y = event.clientY - rect.top
    console.log("x: " + x + " y: " + y)
    return {x, y};
}