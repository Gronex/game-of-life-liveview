export default class Point extends HTMLElement {
    constructor() {
        super();
    }

    get x() {
        return +this.getAttribute("x");
    }

    get y() {
        return +this.getAttribute("y");
    }
}