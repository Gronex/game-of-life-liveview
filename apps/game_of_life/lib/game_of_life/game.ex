defmodule GameOfLife.Game do

  def next(live) do
    live
    |> get_candidates
    |> Enum.filter(fn x -> alive(live, x) end)
    |> MapSet.new()
  end

  def get_candidates(live) do
    live
      |> Enum.flat_map(&get_neighbours/1)
      |> MapSet.new()
      |> MapSet.union(live)
  end

  def alive(cells, cell) do
    neighbour_count = cells
    |> get_live_neighbours(cell)
    |> Enum.count()

    if MapSet.member?(cells, cell) do
      neighbour_count === 2 || neighbour_count === 3
    else
      neighbour_count === 3
    end
  end

  def get_neighbours({x, y}) do
    MapSet.new([
      {x - 1, y - 1},
      {x,     y - 1},
      {x + 1, y - 1},
      {x + 1, y + 1},
      {x,     y + 1},
      {x - 1, y + 1},
      {x + 1, y},
      {x - 1, y},
    ])
  end

  def get_live_neighbours(cells, cell) do
    get_neighbours(cell)
    |> MapSet.intersection(cells)
  end
end
