import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :game_of_life_web, GameOfLifeWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "ofXBiBWUvZuEH9dzKGFXy9wesoFb62WDo6XEQh7U6GqSbAh71vI5SVBdS53zldnE",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# In test we don't send emails.
config :game_of_life, GameOfLife.Mailer, adapter: Swoosh.Adapters.Test

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
